# Word Counter
Word counter je program pro spočítácí četnosti slov v několika souborech najednou.
Nemusí se jednat pouze o slova, ale také můžeme počítat n-gramy.
Zadáním je vytvořit aplikaci, která splňuje tyto možnosti.

```
N-gram je definován jako sled n po sobě jdoucích položek z dané posloupnosti.
Ze sémantického pohledu může být tato posloupnost buď posloupností slov nebo písmen,
nebo čehokoli jiného. V praxi se častěji vyskytují n-gramy jako sled slov.
Zdroj: https://cs.wikipedia.org/wiki/N-gram
```

Zadání tak jak je uvedeno na https://cw.fel.cvut.cz/b181/courses/b6b36pjc/ukoly/semestralka
```
Typický problém s jednoduchou paralelizací. Na vstupu je sada souborů, na výstupu je seřazený výpis obsažených slov a jejich četnost.

Krom slov se dají počítat i tzv. n-gramy.
```

## Implementace
Implementace pro jedno vlákno je triviální, vícevláknová implementace pak může používat pro každý soubor až jedno vlákno.
Například když máme dva vstupní soubory, můžeme použít až dvě vlákna aby se co nejvíce souborů téměř paralelně zpracovávalo.

Implementace také dovoluje místo obyčejných slov počítat také n-gramy,
stačí si v přepínači zvolit místo nuly zvolit požadovanou délku n-gramu. <gramSize> 
N-gramy pokud nesedí délka tak se zbývající prázdné pozice doplní "_".

V argumentech se dají použít dva QoL přepínače, -c a -p, které nám usnadní dostat výsledek jaký chceme.
Pomocí -h se vytiskne nápověda.

Příklad správně zadaných argumentů: 0 2 "testData.txt" "testData2.txt"

Help:
```
usage: wordfreqgramcount [-c] [-p] <gramSize> [auto|<threadCount>] [<fileName>...]
usage: wordfreqgramcount -h
'auto' will use thread count matching the cores on the machine.
gramSize of 0 means to count words, positive gramSize counts grams.
-h prints this help.
-c ignores case.
-p ignores punctuation - treats punctuation as whitespace.
Specify all flags individually before all positional arguments. 
```

Pro linux je nutné kvůli vláknům kompilovat s extra flagem:
```
g++ -Wl,--no-as-needed -std=c++11 -lpthread
```
(Důležitá část je -lpthread, jinak bude hlásit, že multithreading není supported)

## Měření
Měření proběhlo v IDE JetBrains CLion 2018.2.4 vůči kódu v commitu 54910932 (update wordfrequencycount.cpp),
porovnávali jsme rychlost implementace s jedním vláknem oproti implementaci s více vlákny (dvě)

Měření probíhalo na 4 jádrovém i5-4200M CPU taktovaném na 2.5 GHz.

## Výstup z měření

Jedno vlákno:
```
Number of threads used to process the file: 1.
Total running time of word count was: 6635 microseconds
```

Dvě vlákna:
```
Number of threads used to process the file: 2.
Total running time of word count was: 3361 microseconds
```

Porovnáním celkového času běhu můžeme pozorovat, že vícevláknové řešení je znatelně rychlejší.

Výstup z testovacích souborů: ("testData.txt" a "testData2.txt")
```
But: 1
I: 2
a: 1
account: 1
actual: 1
all: 1
and: 3
born: 1
builder: 1
complete: 1
denouncing: 1
explain: 1
explorer: 1
expound: 1
give: 1
great: 1
happiness: 1
how: 1
human: 1
idea: 1
master: 1
mistaken: 1
must: 1
of: 5
pain: 1
pleasure: 1
praising: 1
system: 1
teachings: 1
the: 5
this: 1
to: 1
truth: 1
was: 1
will: 1
you: 2
```
