#include <stdio.h>
#include <iostream>
#include <fstream>
#include <thread>
#include <functional>
#include <vector>
#include <sstream>
#include <exception>
#include <stdio.h>
#include <string.h>
#include <chrono>
#include <ctype.h>
#include <map>
#include <algorithm>

using namespace std::chrono;
using namespace std;

class exceptionProcessingFileChunk : public exception {
private:
    const char *fileName;

public:
    exceptionProcessingFileChunk() {
    }

    exceptionProcessingFileChunk(const char *fileName) : fileName(fileName) {
    }

    virtual const char *what() const throw() {
        return fileName;
    }
};

void openChunkForReading(const char *filePath, ifstream &file) {
    file.open(filePath, ios::binary);

    if (!file.good()) {
        throw exceptionProcessingFileChunk(filePath);
    }
}

bool isWhiteSpace(char c) {
    if (isspace(c)) {
        return true;
    } else {
        return false;
    }
}

void getLatestGram(int gramLength, string word, bool endOfWord, bool endOnly, vector<string> &grams) {
    if (gramLength == 0) {
        // regular words only, no grams
        if (endOfWord) {
            grams.push_back(word);
        }
        return;
    }

    int endOfWordGramOverflow = 1;
    int startOfWordGramOverflow = 0;
    if (endOfWord) {
        endOfWordGramOverflow = gramLength;
    }
    if (endOnly) {
        startOfWordGramOverflow = 1;
    }
    for (int endIndex = startOfWordGramOverflow; endIndex < endOfWordGramOverflow; endIndex++) {

        int wordLength = word.size();
        int substringSize = min(wordLength, gramLength - endIndex);
        int position = word.size() - substringSize;
        string gram = word.substr(position, substringSize);

        // fill in required gram length at the end of the word if necessary
        for (int i = 0; i < endIndex; i++) {
            gram = gram + "_";
        }

        int remainingLentgh = gramLength - gram.size();
        // fill in required gram length at the beginning of the word if necessary
        for (int i = 0; i < remainingLentgh; i++) {
            gram = "_" + gram;
        }
        grams.push_back(gram);
    }
}

void addWord(string word, map<string, long> &wordCounts) {
    map<string, long>::iterator it = wordCounts.find(word);
    if (it != wordCounts.end()) {
        //element found;
        long count = it->second;
        wordCounts[word] = count + 1;
    } else {
        wordCounts.insert(pair<string, long>(word, 1));
    }
}

void addWords(vector<string> words, map<string, long> &wordCounts) {
    for (auto const &word: words) {
        addWord(word, wordCounts);
    }
}

struct Configuration {
    // 0 for words, positive for grams
    int gramLength = 0;
    bool ignoreCase = false;
    bool ignorePunctuation = false;
};

void processChunkInternal(ifstream &chunk, map<string, long> &wordCounts, Configuration config) {
    bool wordContinues = false;
    long bytesProcessed = 0;
    string wordInProgress = "";
    int gramLength = config.gramLength;
    bool ignoreCase = config.ignoreCase;
    bool ignorePunctuation = config.ignorePunctuation;
    while (chunk) {
        char c;
        chunk.get(c);

        if (ignoreCase) {
            c = tolower(c);
        }

        if (chunk) {

            bool whitespace = isWhiteSpace(c);
            if (ignorePunctuation) {
                // treat punctuation as white spaces
                whitespace = whitespace || ispunct(c);
            }

            if (whitespace && !wordContinues) {
                // no word started yet, ignore this whitespace
            } else if (whitespace && wordContinues) {
                // first whitespace after a word, count the word

                vector<string> grams;
                getLatestGram(gramLength, wordInProgress, true, true, grams);
                addWords(grams, wordCounts);

                // reset word started
                wordContinues = false;
                wordInProgress = "";
            } else if (!whitespace && wordContinues) {
                // regular character, another in a word
                wordInProgress = wordInProgress + c;
                vector<string> grams;
                getLatestGram(gramLength, wordInProgress, false, false, grams);
                addWords(grams, wordCounts);
            } else {
                // regular characted, first in a word
                wordInProgress = wordInProgress + c;
                vector<string> grams;
                getLatestGram(gramLength, wordInProgress, false, false, grams);
                addWords(grams, wordCounts);

                // remember we are inside a word now
                wordContinues = true;
            }

            bytesProcessed++;
        }
    }

    if (wordContinues) {
        // last word without a space & must be end of the word, count the word
        vector<string> grams;
        getLatestGram(gramLength, wordInProgress, true, true, grams);
        addWords(grams, wordCounts);
    }

    if (bytesProcessed == 0) {
        // That is not valid input file, something may have gone wrong
        throw exceptionProcessingFileChunk();
    }

}

struct ChunkResult {
    map<string, long> wordCounts;
    bool exceptional = true;
    const char *fileName;
};

void processChunk(vector<const char *> filePaths, vector<ChunkResult> &results, Configuration config) {
    for (auto const &filePath: filePaths) {
        ChunkResult result;
        result.fileName = filePath;
        try {
            ifstream chunk;
            openChunkForReading(filePath, chunk);

            if (chunk.is_open()) {
                processChunkInternal(chunk, result.wordCounts, config);
                result.exceptional = false;
            }
            chunk.close();
        }
        catch (exception &e) {
            // Catch exception now, the code will deal with exceptional results separately
        }
        results.push_back(result);
    }
}

class ThreadManager {
    const int parallelCount;
    vector<vector<ChunkResult>> threadResults;

public:
    ThreadManager(int parallelCount) : parallelCount{parallelCount} {
        this->threadResults = vector<vector<ChunkResult>>(parallelCount);
    }

    void processAllThreads(vector<const char *> filePaths, Configuration config) {
        long chunkSize = filePaths.size() / parallelCount;
        long lastChunkSize = -1;
        vector<thread> threads;

        if (parallelCount == 1) {
            // do not create an extra thread for single thread, use the existing thread
            processChunk(filePaths, this->threadResults.at(0), config);
        } else {
            // start all threads
            for (int i = 0; i < this->parallelCount; i++) {

                if (i == this->parallelCount - 1) {
                    // last chunk
                    vector<const char *> filePathChunk(&filePaths[i * chunkSize], &filePaths[filePaths.size() - 1]);
                    threads.push_back(thread(processChunk, filePathChunk, std::ref(threadResults.at(i)), config));
                } else {
                    vector<const char *> filePathChunk(&filePaths[i * chunkSize],
                                                       &filePaths[i * chunkSize + chunkSize]);
                    threads.push_back(thread(processChunk, filePathChunk, std::ref(threadResults.at(i)), config));
                }
            }
            // wait for all threads to finish
            for (int i = 0; i < this->parallelCount; i++) {
                threads.at(i).join();
            }
        }
    }

    void aggregateWordCount(map<string, long> &wordCounts) {

        for (auto const &threadResult: this->threadResults) {
            for (auto const &chunkResult: threadResult) {
                if (chunkResult.exceptional) {
                    //check exceptional file results
                    throw exceptionProcessingFileChunk(chunkResult.fileName);
                }

                for (auto const &wordRecord: chunkResult.wordCounts) {
                    map<string, long>::iterator it = wordCounts.find(wordRecord.first);
                    if (it != wordCounts.end()) {
                        //element found;
                        long count = it->second;
                        wordCounts[wordRecord.first] = count + wordRecord.second;
                    } else {
                        wordCounts.insert(pair<string, long>(wordRecord.first, wordRecord.second));
                    }

                }
            }

        }
    }
};


void printHelp() {
    printf("usage: wordfreqgramcount [-c] [-p] <gramSize> [auto|<threadCount>] [<fileName>...]\n");
    printf("usage: wordfreqgramcount -h\n");
    printf("'auto' will use thread count matching the cores on the machine.\n");
    printf("gramSize of 0 means to count words, positive gramSize counts grams.\n");
    printf("-h prints this help.\n");
    printf("-c ignores case.\n");
    printf("-p ignores punctuation - treats punctuation as whitespace.\n");
    printf("Specify all flags individually before all positional arguments.\n");
}

bool checkArgForHelp(const char *arg) {
    if (strcmp(arg, "--help") == 0 || strcmp(arg, "-h") == 0 || strcmp(arg, "-Help") == 0 || strcmp(arg, "-?") == 0) {
        return true;
    }
    return false;
}

int treatFlags(int argc, char *argv[], Configuration &config) {

    int i = 1;
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-c") == 0) {
            config.ignoreCase = true;
        } else if (strcmp(argv[i], "-p") == 0) {
            config.ignorePunctuation = true;
        } else {
            break;
        }
    }
    // return the next arg index to process
    return i;
}

int readNonNegInt(const char *arg) {
    int result;
    istringstream iss(arg);

    if (!(iss >> result)) {
        printf("Expected int as argument, got: %s.\n", arg);
        printHelp();
        return -1;
    }
    if (!iss.eof()) {
        printf("Expected int as argument, got: %s.\n", arg);
        printHelp();
        return -1;
    }

    if (result < 0) {
        printf("Expected non-negative int as argument, got: %s.\n", arg);
        printHelp();
        return -1;
    }
    return result;
}

int main(int argc, char *argv[]) {
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    int maxThreadCount = 100; // limit some crazyness
    int parallelCount;
    bool autocount = false;
    vector<const char *> filePaths;

    Configuration config;
    int nextArg = treatFlags(argc, argv, config);

    if (argc - nextArg == 1) {
        // help
        const char *argument = argv[nextArg];
        if (checkArgForHelp(argument)) {
            printHelp();
            return 0;
        } else {
            printf("Unrecognized arguments.\n");
            printHelp();
            return 1;
        }

    } else if (argc - nextArg >= 3) {
        int gramConfig = readNonNegInt(argv[nextArg]);
        if (gramConfig == -1) {
            return 1;
        }
        config.gramLength = gramConfig;
        nextArg++;

        if (strcmp(argv[nextArg], "auto") == 0) {
            // use auto thread count
            parallelCount = thread::hardware_concurrency();
            autocount = true;
        } else {
            // non default and non auto thread count
            parallelCount = readNonNegInt(argv[nextArg]);
            if (parallelCount == -1) {
                return 1;
            }
        }
        nextArg++;
        for (int i = nextArg; i < argc; i++) {
            filePaths.push_back(argv[i]);
        }

    } else {
        printf("Wrong number of arguments.\n");
        printHelp();
        return 1;

    }
    if (autocount) {
        if (filePaths.size() < parallelCount) {
            parallelCount = filePaths.size();
        }
    }

    if (filePaths.size() < parallelCount || parallelCount > maxThreadCount) {
        printf("Requested too many threads. Maximum overall possible thread count is: %d. Or too little files for given thread count. Count of files: %d, requested thread count: %d.\n",
               maxThreadCount, filePaths.size(), parallelCount);
        printHelp();
        return 1;
    }
    if (parallelCount < 1) {
        printf("Requested thread count must be positive.\n");
        printHelp();
        return 1;
    }

    ThreadManager t = ThreadManager(parallelCount);

    t.processAllThreads(filePaths, config);

    map<string, long> wordCounts;
    try {
        t.aggregateWordCount(wordCounts);

    } catch (exceptionProcessingFileChunk &e) {
        printf("Could not read the file '%s' correctly. File may not exist of be of a wrong type.\n", e.what());
        printHelp();
        return 1;
    }

    printf("Number of threads used to process the file: %d.\n", parallelCount);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(t2 - t1).count();
    printf("Total running time of word count was: %d microseconds\n", duration);

    for (auto const &wordRecord: wordCounts) {
        cout << wordRecord.first << ": " << wordRecord.second << endl;
    }

    return 0;
}
